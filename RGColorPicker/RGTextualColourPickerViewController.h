//
//  RGTextualColourPickerViewController.h
//  Foliobook
//
//  Created by Paul on 10/01/2014.
//
//

#import <UIKit/UIKit.h>
#import "RGColorController.h"
#import "ColorSelectorViewController.h"
#import "ColourModelSelectorViewController.h"
#import "RGPropertyNotification.h"

enum RGColorModel {
    RGhsv=0,  RGrgb = 1, RGweb=2
};

@class ColourModelSelectorViewController;

#define LAST_SELECTED_COLOR_MODEL_DEFAULT @"last-selected-color-model"

@interface RGTextualColourPickerViewController : ColorSelectorViewController <RGColorController>

@property enum RGPropertyNotificationScope  notificationScope;
@property int colorModel;
@property BOOL notificationsEnabled;

@property (weak, nonatomic) IBOutlet UIView *colorPatchView;
@property (strong, nonatomic) IBOutlet UIView *colorPatchBackgroundView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *colorModelSegmentedControl;

@property (weak, nonatomic) IBOutlet UIView *rgbContainerView;
@property (weak, nonatomic) IBOutlet UIView *hsvContainerView;
@property (weak, nonatomic) IBOutlet UIView *webContainerView;

@property (strong, nonatomic) ColourModelSelectorViewController *hsvController;
@property (strong, nonatomic) ColourModelSelectorViewController *rgbController;
@property (strong, nonatomic) ColourModelSelectorViewController *webController;
 

- (IBAction)colorModelChanged:(id)sender;
-(void) colorChanged:(UIColor *) color;
 

@end
