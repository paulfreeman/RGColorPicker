//
//  ColorSelectorViewController.m
//  Foliobook
//
//  Created by Paul on 12/01/2014.
//
//

#import "ColorSelectorViewController.h"
#import "RGPropertyNotification.h"

@interface ColorSelectorViewController ()

@end

@implementation ColorSelectorViewController

-(void) viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor lightGrayColor];
} 


@end
