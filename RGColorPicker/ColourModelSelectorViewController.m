//
//  ColourModelSelectorViewController.m
//  Foliobook
//
//  Created by Paul on 13/01/2014.
//
//

#import "ColourModelSelectorViewController.h"

@interface ColourModelSelectorViewController ()

@end

@implementation ColourModelSelectorViewController


-(void) viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor lightGrayColor];
}

//abstract, must be implemented by a subclass
-(void) populateControls {}

@end
