//
//  HueSlidersViewController.m
//  Foliobook
//
//  Created by Paul on 12/01/2014.
//
//

#import "HueSlidersViewController.h"

@interface HueSlidersViewController ()

@end

@implementation HueSlidersViewController

#pragma mark --
#pragma mark population 

-(void) populateControls {
    
    UIColor *c =  self.textualDelegate.delegate.currentlySelectedColor ;
    if(IsEmpty(c)){
        c = [UIColor whiteColor];
    }
    
    CGFloat hue, saturation, brightness, alpha;
    [c getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    
    self.slider1.value = hue;
    self.tf1.text = [NSString stringWithFormat:@"%2.0f",  hue*255.0];
    
    self.slider2.value = saturation;
    self.tf2.text = [NSString stringWithFormat:@"%2.0f",  saturation*255.0];
    
    self.slider3.value = brightness;
    self.tf3.text = [NSString stringWithFormat:@"%2.0f",  brightness*255.0];
    
    self.slider4.value = alpha;
    self.tf4.text = [NSString stringWithFormat:@"%2.0f",  alpha*255.0];
    
}

#pragma mark --
#pragma mark text

-(void)keyboardWillShow:(NSNotification *) notification {
    if(!self.keyboardIsDisplayed){
        [self moveTextViewForKeyboardUp:YES];
        self.keyboardIsDisplayed = YES;
    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [self moveTextViewForKeyboardUp:NO];
    self.keyboardIsDisplayed = NO;
}

- (void) moveTextViewForKeyboardUp:(BOOL) isUp {
    // Animate up or down
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    
    CGRect newFrame = self.view.frame;
    
    if(isUp){
        newFrame.origin.y = newFrame.origin.y - self.viewAnimationDistance;
    }
    else {
        newFrame.origin.y = newFrame.origin.y + self.viewAnimationDistance;
        // newFrame = CGRectMake(0, 0, newFrame.size.width, newFrame.size.height);
    }
    
    self.view.frame = newFrame;
    
    [UIView commitAnimations];
}


#pragma mark --
#pragma mark lifecycle

-(void) viewWillAppear:(BOOL)animated {
    [self populateControls];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.-(void)
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
    //self.viewAnimationDistance = 125;
    //self.keyboardIsDisplayed = NO;
}
 

#pragma mark -- 
#pragma mark actions


-(IBAction) colourChanged {
    UIColor *c = [UIColor colorWithHue:self.slider1.value saturation:self.slider2.value brightness:self.slider3.value alpha:self.slider4.value];
    [self.textualDelegate  colorChanged:c];
}


- (IBAction)slider1changed:(id)sender {
    self.tf1.text = [NSString stringWithFormat:@"%2.0f", self.slider1.value*255.0];
    [self colourChanged];
}

- (IBAction)slider2changed:(id)sender {
    self.tf2.text = [NSString stringWithFormat:@"%2.0f", self.slider2.value*255.0];
    [self colourChanged];
}

- (IBAction)slider3changed:(id)sender {
    self.tf3.text = [NSString stringWithFormat:@"%2.0f", self.slider3.value*255.0];
    [self colourChanged];
}

- (IBAction)slider4changed:(id)sender {
    self.tf4.text = [NSString stringWithFormat:@"%2.0f", self.slider4.value*255.0];
    [self colourChanged];
}

- (IBAction)tf1changed:(id)sender {
    CGFloat colourValue = [self.tf1.text floatValue]/255.0;
    self.slider1.value = colourValue;
    [self colourChanged];
}

- (IBAction)tf2changed:(id)sender {
    CGFloat colourValue = [self.tf2.text floatValue]/255.0;
    self.slider2.value = colourValue;
    [self colourChanged];
}

- (IBAction)tf3changed:(id)sender {
    CGFloat colourValue = [self.tf3.text floatValue]/255.0;
    self.slider3.value = colourValue;
    [self colourChanged];
}

- (IBAction)tf4changed:(id)sender {
    CGFloat colourValue = [self.tf4.text floatValue]/255.0;
    self.slider4.value = colourValue;
    [self colourChanged];
}




@end
