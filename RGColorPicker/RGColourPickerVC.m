//
//  TestColorViewController.m
//  RSColorPicker
//
//  Created by Ryan Sullivan on 7/14/13.
//  Copyright (c) 2013 Freelance Web Developer. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "RGColourPickerVC.h"
#import "RSBrightnessSlider.h"
#import "RSOpacitySlider.h"

@import foliobookcore;
#import "RGPropertyNotification.h"
#import "CoreDataController.h"

@implementation RGColourPickerVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void) displayCurrentColorSelection {
    
    UIColor *c =  self.delegate.currentlySelectedColor ;
    if(IsEmpty(c)){
        self.delegate.currentlySelectedColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
        c = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    }
    CGFloat hue, saturation, brightness, transparency;
    [c getHue:&hue saturation:&saturation brightness:&brightness alpha:&transparency];
    
    //_//colorPatch.backgroundColor = c;
    //_colorPatch.alpha       = transparency;
    //_brightnessSlider.value = 1.0;
    self.colorPicker.selectionColor = c;
    _opacitySlider.value    = transparency;
    _brightnessSlider.value = brightness;
    _colorPatch.backgroundColor = c;
    
}

-(void) viewDidAppear:(BOOL)animated {
    self.notificationsEnabled = YES;
}

-(void) viewWillAppear:(BOOL)animated {
    self.notificationsEnabled = NO;
    [self displayCurrentColorSelection];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    self.navigationController.navigationBar.translucent = NO;
    
    //transparent underlay _colorPatch =
     self.colorPatchBackgroundView  = [[UIView alloc] initWithFrame:CGRectMake(30, 25, 260, 40.0)];
     self.colorPatchBackgroundView.layer.cornerRadius = 4;
    [self.view addSubview: self.colorPatchBackgroundView];
    UIImage *backgroundImage = RSOpacityBackgroundImage(16.f, [UIColor colorWithWhite:0.5 alpha:1.0]);
    self.colorPatchBackgroundView.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    self.colorPatchBackgroundView.alpha = 0.2;
    
    // View that shows selected color
    _colorPatch = [[UIView alloc] initWithFrame:CGRectMake(30, 25, 260, 40.0)];
    _colorPatch.layer.cornerRadius = 4;
    [self.view addSubview:_colorPatch];
    
    
     _rgbLabel = [[UILabel alloc] initWithFrame:CGRectMake(90, 25, 180, 30)];
    _rgbLabel.textAlignment = NSTextAlignmentCenter;
     //[self.view addSubview:_rgbLabel];
    
    // View that displays color picker (needs to be square)
    _colorPicker = [[RSColorPickerView alloc] initWithFrame:CGRectMake(30.0, 85.0, 260.0, 260.0)];
    
    // Optionally set and force the picker to only draw a circle
//    [_colorPicker setCropToCircle:YES]; // Defaults to NO (you can set BG color)
    
    // Set the selection color - useful to present when the user had picked a color previously
    
//    [_colorPicker setSelectionColor:[UIColor colorWithRed:1 green:0 blue:0.752941 alpha:1.000000]];
//    [_colorPicker setSelection:CGPointMake(269, 269)];
    
    // Set the delegate to receive events
    [_colorPicker setDelegate:self];
    
    [self.view addSubview:_colorPicker];
    
    // View that controls brightness
    _brightnessSlider = [[RSBrightnessSlider alloc] initWithFrame:CGRectMake(30, 365.0, 260, 20.0)];
    [_brightnessSlider setColorPicker:_colorPicker];
    [self.view addSubview:_brightnessSlider];
    
    _opacitySlider = [[RSOpacitySlider alloc] initWithFrame:CGRectMake(30, 405.0, 260, 20.0)];
    [_opacitySlider setColorPicker:_colorPicker];
     
    [self.view addSubview:_opacitySlider];
    
}

#pragma mark - RSColorPickerView delegate methods

- (void)colorPickerDidChangeSelection:(RSColorPickerView *)cp {
    
    if(!self.notificationsEnabled){
        return;
    }
    
    // Get color data
    UIColor *color = [cp selectionColor];

    CGFloat r, g, b, a;
    [[cp selectionColor] getRed:&r green:&g blue:&b alpha:&a];

    // Update important UI
    _colorPatch.backgroundColor = color;
    _brightnessSlider.value = [cp brightness];
    
    //Need to use the RGB values to get Hue and Sat then combine with B value
    CGFloat hue, saturation, ignoredbrightness, alpha;
    [color  getHue:&hue saturation:&saturation brightness:&ignoredbrightness alpha:&alpha];
     
    //Share with parent/tab controller
    self.delegate.currentlySelectedColor = color;
    
    // old school
    if(self.notificationScope == RGPropertyNotificationScopeBackground){
        //Direct update here
         NSString *hexColor = [PFLFColor hexStringFromColor:color];
        [[CoreDataController sharedInstance ] setComponentProp:hexColor forKey:BACKGROUND_COLOR withComponentName:self.selectedComponent.componentName];
        [[NSNotificationCenter defaultCenter] postNotificationName:BACKGROUND_COLOR_CHANGED_NOTIFICATION object:color];
    }
    else if(self.notificationScope ==  RGPropertyNotificationScopeSelectionFill) {
        RGPropertyNotification *notification = [RGPropertyNotification propertyNotificationWithColor:color scope:self.notificationScope];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:RGPropertyNotificationColorChanged object:notification];
    }
    else if(self.notificationScope == RGPropertyNotificationScopeMenu
        || self.notificationScope == RGPropertyNotificationScopePage){
        
        RGPropertyNotification *cnotify = [RGPropertyNotification propertyNotificationWithColor:color scope: self.notificationScope];
        [[NSNotificationCenter defaultCenter] postNotificationName: RGPropertyNotificationMenuFontColorChanged object:cnotify];
        
    }
    else {
        RGPropertyNotification *notification = [RGPropertyNotification propertyNotificationWithColor:color scope :self.notificationScope];
        [[NSNotificationCenter defaultCenter] postNotificationName:RGPropertyNotificationColorChanged object:notification];
      
    }
    
    // Debug
    int ir = r * 255;
    int ig = g * 255;
    int ib = b * 255;
    NSString * colorDesc = [NSString stringWithFormat:@"rgb: %d, %d, %d", ir, ig, ib];
    _rgbLabel.text = colorDesc;
     
}

#pragma mark - User action


@end
