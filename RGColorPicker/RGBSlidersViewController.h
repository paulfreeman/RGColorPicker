//
//  RGBSlidersViewController.h
//  Foliobook
//
//  Created by Paul on 12/01/2014.
//
//

#import <UIKit/UIKit.h>
#import "ColourModelSelectorViewController.h"

@interface RGBSlidersViewController : ColourModelSelectorViewController

@property (weak, nonatomic) IBOutlet UISlider *slider1;
@property (weak, nonatomic) IBOutlet UITextField *tf1;
@property (weak, nonatomic) IBOutlet UISlider *slider2;
@property (weak, nonatomic) IBOutlet UITextField *tf2;
@property (weak, nonatomic) IBOutlet UISlider *slider3;
@property (weak, nonatomic) IBOutlet UITextField *tf3;
@property (weak, nonatomic) IBOutlet UISlider *slider4;
@property (weak, nonatomic) IBOutlet UITextField *tf4;

- (IBAction)slider1changed:(id)sender;
- (IBAction)slider2changed:(id)sender;
- (IBAction)slider3changed:(id)sender;
- (IBAction)slider4changed:(id)sender;

- (IBAction)tf1changed:(id)sender;
- (IBAction)tf2changed:(id)sender;
- (IBAction)tf3changed:(id)sender;
- (IBAction)tf4changed:(id)sender;

@end
