//
//  ColourModelSelectorViewController.h
//  Foliobook
//
//  Created by Paul on 13/01/2014.
//
//

#import <UIKit/UIKit.h>
#import "RGTextualColourPickerViewController.h"

@class  RGTextualColourPickerViewController;

@interface ColourModelSelectorViewController : UIViewController

@property (nonatomic, weak) RGTextualColourPickerViewController *textualDelegate;

-(void) populateControls;

@end
