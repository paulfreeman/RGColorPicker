//
//  RGImageEditorViewController.m
//  Foliobook
//
//  Created by Paul on 07/03/2014.
//
//

#import "RGImageEditorViewController.h"
#import "RGSourcePickerViewController.h"
#import "IOTypes.h"
#import "IOUtil.h"
#import "CloudFilePicker.h"
#import "DropboxFilePicker.h"
#import "PhotoshelterCloudPickerSource.h"
#import "DropboxImportItem.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ImageUtils.h"
#import "Utils.h"
#import "CoreDataController.h"
#import "RGAlphaSlider.h"
#import "FileCacheUtils.h"

@interface RGImageEditorViewController ()

@end

@implementation RGImageEditorViewController


#pragma mark --
#pragma mark Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.alphaSlider = [[RSOpacitySlider alloc] initWithFrame:CGRectMake(30, 200, 260, 30.0)];
        [self.view addSubview: self.alphaSlider];
        [self.alphaSlider addTarget:self action:@selector(alphaChanged:) forControlEvents:UIControlEventValueChanged];
        
        self.view.backgroundColor = [UIColor lightGrayColor];
    }
    return self;
}


#pragma mark --
#pragma mark Implement RGEditorPaneProtocol
-(void) setSelectedComponent:(LucidaPortfolioComponent *) component {
    //
} 
-(void) setNotificationScope:(enum RGPropertyNotificationScope) scope{
    //
}


#pragma mark --
#pragma mark Populate controls  

-(void) displayBackgroundThumbnail:(UIImageView *) thumbnail withPathKey:(NSString *) pathKey  withFileKey:(NSString *) fileKey withFileType:(NSString *) fileType {
    CoreDataController *defaults = [CoreDataController sharedInstance];
    
    NSString *filepathProp =  [defaults findComponentPropVal:self.contextComponent.parentComponent.componentName   forKey:pathKey];
    NSString *filekeyProp  =  [defaults findComponentPropVal:self.contextComponent.parentComponent.componentName   forKey:fileKey];
    NSString *filetypeProp =  [defaults findComponentPropVal:self.contextComponent.parentComponent.componentName   forKey:fileType];
    
    UIImage *img =  [Utils getResourceThumbnail: filepathProp
                                       withName: filekeyProp
                                       withType: filetypeProp
                                       isHidden: YES];
    
    if(!IsEmpty(img)){
        thumbnail.image = img;
    }
    else {
        thumbnail.image = [UIImage imageNamed:@"nopicture.png"];
    }
}

-(void) populateTransparency {
    NSString *alphaProp =  [[CoreDataController sharedInstance] findComponentPropVal:self.contextComponent.parentComponent.componentName   forKey:BACKGROUND_IMAGE_ALPHA];
    if(IsEmpty(alphaProp)){
        alphaProp = @"1.0";
    }
    float alpha = [alphaProp floatValue];
    self.alphaSlider.value = alpha;
}

-(void) populateImageSizing {
    int state;
    NSString *aspectDefault = [[NSString alloc] initWithFormat:@"%ld",  (long)UIViewContentModeScaleAspectFill];
    
    state = [[[CoreDataController sharedInstance] findComponentPropVal:self.contextComponent.parentComponent.componentName forKey:BACKGROUND_IMAGE_SIZING withDefaultValue:aspectDefault] intValue];
    self.aspectSegmentedControl.selectedSegmentIndex = state; 
}

-(void) populate {
    [self displayBackgroundThumbnail:self.thumbnailIV withPathKey:BACKGROUND_IMAGE_FILE_PATH  withFileKey:BACKGROUND_IMAGE_FILE_NAME  withFileType:BACKGROUND_IMAGE_FILE_TYPE];
    [self populateTransparency];
    [self populateImageSizing];
}


#pragma mark --
#pragma mark Lifecycle methods

-(void) viewWillAppear:(BOOL)animated {
    [self populate];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark Delegate implementations 

//UIImagePickerController Delegate
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //UIPopoverController *mypop =  (UIPopoverController *) [self.navigationController parentViewController];
    //[mypop setContentViewController:self.parentNavController];
}

//Cloudfilepicker delegate impl
#pragma mark --
#pragma mark Actions

- (IBAction)selectImage:(id) sender  {
    RGSourcePickerViewController *picker = [[RGSourcePickerViewController alloc]  initWithStyle:UITableViewStyleGrouped];
    picker.maxPicks = 1;
    picker.pickerDelegate = (id<RGPickerDelegate,CloudFilePickerDelegate>) self;
	picker.pickerMode = RGPickerModeSingleImage;
	picker.mediaType=RGMediaTypeImage;
	[self.navigationController pushViewController: picker animated:YES];
}

#pragma mark --
#pragma mark Cloud File Picker Delegate impl
-(void) displayCloudFilePicker{
    DropboxFilePicker *cpicker = [[DropboxFilePicker alloc] initWithNibName:@"DropboxFilePicker" bundle:nil];
    
    cpicker.contextName = self.contextComponent.parentComponent.componentName;
    cpicker.parentCollection = DROPBOX_ROOT_FOLDER;
    cpicker.parentController = self;
    cpicker.maxPicks = 1;
    cpicker.mediaType= RGMediaTypeAny;
    cpicker.pickerMode=RGPickerModeSingleImage;
    cpicker.sourceType=RGDropbox;
    cpicker.allowsSync=NO; //override default
    
    UINavigationController *navController = [[UINavigationController alloc]
                                             initWithRootViewController:cpicker];
    navController.modalPresentationStyle=UIModalPresentationFormSheet;
    navController.navigationBarHidden = NO;
    [self presentViewController:navController animated:YES completion:^{}];
}


-(void) displayPhotoshelterPicker {
    CloudFilePicker *cpicker = [[CloudFilePicker alloc] initWithNibName:@"DropboxFilePicker" bundle:nil];
    
    cpicker.contextName = self.contextComponent.parentComponent.componentName;
    cpicker.parentController = self;
    cpicker.maxPicks = 1;
    cpicker.mediaType= RGMediaTypeAny;
    cpicker.pickerMode=RGPickerModeSingleImage;
    cpicker.sourceType=RGPhotoShelter;
    cpicker.allowsSync=NO;
    cpicker.cloudPickerSource    = [PhotoshelterCloudPickerSource sharedSource];
    cpicker.cloudPickerSSLSource = [PhotoshelterCloudPickerSource sharedSSLSource];
    
    UINavigationController *navController = [[UINavigationController alloc]
                                             initWithRootViewController:cpicker];
    navController.modalPresentationStyle=UIModalPresentationFormSheet;
    navController.navigationBarHidden = NO;
    [self presentViewController:navController animated:YES completion:^{}]; 
}
 

#pragma mark --
#pragma mark Foo Delegate implementation

- (void) pickerFinishedPickingMedia:(NSArray *) assetArray {
    if(IsEmpty(assetArray)){
        return;
    }
    else {
        ALAsset *theAsset = [assetArray objectAtIndex:0];
        if(!IsEmpty(theAsset)){
            //NSString *assetUrl = [[[theAsset valueForProperty:ALAssetPropertyURLs] valueForKey:[[[theAsset valueForProperty:ALAssetPropertyURLs] allKeys] objectAtIndex:0]]   absoluteString];
            //create a thumbnail
            UIImage *img = [UIImage imageWithCGImage:[[theAsset defaultRepresentation] fullResolutionImage]];
            
            //Asset stuff is finished with here
           [self loadThumbnailsFromImage:img];
        }
    }
}

- (void) pickerDismissed {
    [self dismissViewControllerAnimated:YES completion:^{}];
}

//Alternative non Assets Library based result.  Use the first item
-(void) pickerLoadedMedia:(NSArray *)array {
    DropboxImportItem *theItem = [array objectAtIndex:0];
    if(theItem==nil){
        return;
    }
    else {
        UIImage *img = [UIImage imageWithContentsOfFile:[IOUtil getDownloadDestinationPathForUUFilename:theItem.uufilename]];
        
        //resize to max background size.
        [self loadThumbnailsFromImage:img];
        
        //Clean up the full file loaded from the cloudpicker as a side effect
        //of the loading operation.
#pragma mark RGImageEditorViewControllertodo fix FileCacheUtils
        //[FileCacheUtils removeFilesForId:theItem.uufilename];
    }
}


-(void) pickerEncounteredError:(NSError *)error {
    /*
    HelpView *viewController = [[HelpView alloc] initWithNibName: @"HelpView" bundle:nil];
    viewController.showsDoneButton=YES;
    viewController.delegate = self;
    UINavigationController *navController = [[UINavigationController alloc]
                                             initWithRootViewController:viewController];
    navController.modalPresentationStyle=UIModalPresentationFormSheet;
    [self presentViewController:navController animated:YES completion:^{}];
#ifdef FOLIOBOOK
    [viewController showLocal:@"locationwarn_foliobook" ofType:@"html" ];
#endif
#ifdef CREATIVEPORTFOLIO
    [viewController showLocal:@"locationwarn_creative" ofType:@"html" ];
#endif
    
#ifdef SHOWREEL
    [viewController showLocal:@"locationwarn_showreel" ofType:@"html" ];
#endif
     */
}


//todo factor out common code in BackgroundEditor to Utils class

-(void) loadThumbnailsFromImage:(UIImage *) img {
    
    NSData *sourceData = UIImageJPEGRepresentation(img, 1.0);
    //Process thumbnail
    NSData *thumbData = [ImageUtils getResizedImageDataForJPEG:sourceData withLongEdge:LIGHTBOX_THUMBNAIL_SIZE withJPEGQuality:0.6];
    
    //Save the thumbnail as the standard video thumbnail name.
    //Create a directory for backgrounds and thumbs if one does not yet exist
    NSString *backgroundDirectoryPath = [Utils getBackgroundDirectoryPath];
    //NSLog(@"Background dir path = %@", backgroundDirectoryPath);
    NSString *thmPath  =  nil;
    NSString *imgPath  =  nil;
    
    //TODO clean up representation of background video file name
    CoreDataController *defaults = [CoreDataController sharedInstance] ;
         
    [defaults setComponentProp:@"0" forKey:BACKGROUND_IMAGE_FROM_BUNDLE withComponentName:self.contextComponent.parentComponent.componentName];
    
    NSString *rootFileName = [[NSString alloc] initWithFormat:@"%@_%@" , self.contextComponent.parentComponent.componentName, SAVED_BACKGROUND_NAME ];
    
    thmPath = [[NSString alloc] initWithFormat:@"%@/.%@%@.%@", backgroundDirectoryPath, rootFileName, THUMBNAIL_FILE_SUFFIX, THUMBNAIL_FILE_TYPE ];
    imgPath = [[NSString alloc] initWithFormat:@"%@/.%@.%@",   backgroundDirectoryPath, rootFileName, THUMBNAIL_FILE_TYPE ];
    
    // NSLog(@"putting landscape %@ for parent %@",imgPath,  self.subcategoryName);
    
    [defaults setComponentProp:backgroundDirectoryPath   forKey:BACKGROUND_IMAGE_FILE_PATH  withComponentName:self.contextComponent.parentComponent.componentName];
    [defaults setComponentProp:rootFileName              forKey:BACKGROUND_IMAGE_FILE_NAME  withComponentName:self.contextComponent.parentComponent.componentName];
    [defaults setComponentProp:THUMBNAIL_FILE_TYPE          forKey:BACKGROUND_IMAGE_FILE_TYPE  withComponentName:self.contextComponent.parentComponent.componentName];
    if (![sourceData writeToFile:imgPath atomically:YES]){
        NSLog(@" there was an error writing the image to the docs directory");
    }
    if (![thumbData writeToFile:thmPath atomically:YES]){
        NSLog(@" there was an error writing the thumbnail to the docs directory");
    }
  
    [[NSNotificationCenter defaultCenter] postNotificationName:SELECTION_BACKGROUND_CHANGED_NOTIFICATION object:nil];
    
    [self populate];
    
    [self.navigationController popToViewController:self animated:YES];
    CGRect rect = CGRectMake(0, 0, POPOVER_WIDTH_IPAD, POPOVER_HEIGHT_IPAD);
    [self setPreferredContentSize:rect.size];
}


- (IBAction)alphaChanged:(id)sender {
     float val = ((RGAlphaSlider *) sender).value;
    
     [[CoreDataController sharedInstance] setComponentProp:[NSString stringWithFormat:@"%2f", val] forKey:BACKGROUND_IMAGE_ALPHA withComponentName:self.contextComponent.parentComponent.componentName];
    
	 NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	 [nc postNotificationName:SELECTION_BACKGROUND_CHANGED_NOTIFICATION object:nil];
}


- (IBAction)aspectChanged:(id)sender {
    NSString *selectedSegIdx = [[NSString alloc] initWithFormat:@"%ld", (long)self.aspectSegmentedControl.selectedSegmentIndex ];
 
    [[CoreDataController sharedInstance] setComponentProp:selectedSegIdx  forKey:BACKGROUND_IMAGE_SIZING withComponentName:self.contextComponent.parentComponent.componentName];
    
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc postNotificationName:SELECTION_BACKGROUND_CHANGED_NOTIFICATION object:nil];
}

@end
