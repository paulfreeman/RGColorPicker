//
//  ColorUtils.m
//  LucidaFolio
//
//  Created by Paul Freeman on 21/06/2010.
//  Copyright 2010 Rocket Garden Labs Ltd All rights reserved.
//

#import "PFLFColor.h"

@implementation PFLFColor
 
+ (CGColorSpaceModel)colorSpaceModel:(UIColor *) color {
	return CGColorSpaceGetModel(CGColorGetColorSpace(color.CGColor));
}

+ (BOOL) canProvideRGBComponents :(UIColor *) color
{
	return (([PFLFColor colorSpaceModel:color] == kCGColorSpaceModelRGB) || 
			([PFLFColor colorSpaceModel:color] == kCGColorSpaceModelMonochrome));
}
 

+ (CGFloat) red :(UIColor *) color
{
	const CGFloat *c = CGColorGetComponents(color.CGColor);
	return c[0];
}

+ (CGFloat) green :(UIColor *) color
{
	const CGFloat *c = CGColorGetComponents(color.CGColor);
	if ([PFLFColor colorSpaceModel:color] == kCGColorSpaceModelMonochrome) return c[0];
	return c[1];
}

+ (CGFloat) blue :(UIColor *) color
{
	const CGFloat *c = CGColorGetComponents(color.CGColor);
	if ([PFLFColor colorSpaceModel:color] == kCGColorSpaceModelMonochrome) return c[0];
	return c[2];
}

+ (CGFloat) alpha: (UIColor *) color
{
	const CGFloat *c = CGColorGetComponents(color.CGColor);
	return c[CGColorGetNumberOfComponents(color.CGColor)-1];
} 

+(NSString *) hexStringFromRed:(CGFloat)r green:(CGFloat) g blue:(CGFloat) b{
	// Fix range if needed  
    if (r < 0.0f) r = 0.0f;  
	if (g < 0.0f) g = 0.0f;  
	if (b < 0.0f) b = 0.0f;  
	
	if (r > 1.0f) r = 1.0f;  
	if (g > 1.0f) g = 1.0f;  
	if (b > 1.0f) b = 1.0f;  
	
	// Convert to hex string between 0x00 and 0xFF  
	return [NSString stringWithFormat:@"%02X%02X%02X",  
			(int)(r * 255), (int)(g * 255), (int)(b * 255)];  
}


+ (NSString *) hexStringFromColor:(UIColor *) color {    

    CGFloat r, g, b;  
    r = [PFLFColor red:color];  
    g = [PFLFColor green:color];  
    b = [PFLFColor blue:color];  
	
    // Fix range if needed  
    if (r < 0.0f) r = 0.0f;  
		if (g < 0.0f) g = 0.0f;  
			if (b < 0.0f) b = 0.0f;  
				
				if (r > 1.0f) r = 1.0f;  
					if (g > 1.0f) g = 1.0f;  
						if (b > 1.0f) b = 1.0f;  
							
							// Convert to hex string between 0x00 and 0xFF  
							return [NSString stringWithFormat:@"%02X%02X%02X",  
									(int)(r * 255), (int)(g * 255), (int)(b * 255)];  
}  

 
+ (UIColor *) colorWithHexString: (NSString *) stringToConvert  
{  
    NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];  
	
    // String should be 6 or 8 characters  
    if ([cString length] < 6) return DEFAULT_VOID_COLOR;  
	
    // strip 0X if it appears  
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];  
	
    if ([cString length] != 6) return DEFAULT_VOID_COLOR;  
	
    // Separate into r, g, b substrings  
    NSRange range;  
    range.location = 0;  
    range.length = 2;  
    NSString *rString = [cString substringWithRange:range];  
	
    range.location = 2;  
    NSString *gString = [cString substringWithRange:range];  
	
    range.location = 4;  
    NSString *bString = [cString substringWithRange:range];  
	
    // Scan values  
    unsigned int r, g, b;  
    [[NSScanner scannerWithString:rString] scanHexInt:&r];  
    [[NSScanner scannerWithString:gString] scanHexInt:&g];  
    [[NSScanner scannerWithString:bString] scanHexInt:&b];  
	
    return [UIColor colorWithRed:((float) r / 255.0f)  
                           green:((float) g / 255.0f)  
                            blue:((float) b / 255.0f)  
                           alpha:1.0f];  
}  
 


- (void)scaleAndRotateImage:(UIImage *)image
{
	int kMaxResolution = 320; // Or whatever
	
	CGImageRef imgRef = image.CGImage;
	
	CGFloat width = CGImageGetWidth(imgRef);
	CGFloat height = CGImageGetHeight(imgRef);
	
	CGAffineTransform transform = CGAffineTransformIdentity;
	CGRect bounds = CGRectMake(0, 0, width, height);
	if (width > kMaxResolution || height > kMaxResolution) {
		CGFloat ratio = width/height;
		if (ratio > 1) {
			bounds.size.width = kMaxResolution;
			bounds.size.height = bounds.size.width / ratio;
		}
		else {
			bounds.size.height = kMaxResolution;
			bounds.size.width = bounds.size.height * ratio;
		}
	}
	
	CGFloat scaleRatio = bounds.size.width / width;
	CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
	CGFloat boundHeight;
	UIImageOrientation orient = image.imageOrientation;
	switch(orient) {
			
		case UIImageOrientationUp: //EXIF = 1
			transform = CGAffineTransformIdentity;
			break;
			
		case UIImageOrientationUpMirrored: //EXIF = 2
			transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			break;
			
		case UIImageOrientationDown: //EXIF = 3
			transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
			
		case UIImageOrientationDownMirrored: //EXIF = 4
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
			transform = CGAffineTransformScale(transform, 1.0, -1.0);
			break;
			
		case UIImageOrientationLeftMirrored: //EXIF = 5
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
			
		case UIImageOrientationLeft: //EXIF = 6
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
			
		case UIImageOrientationRightMirrored: //EXIF = 7
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeScale(-1.0, 1.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
			
		case UIImageOrientationRight: //EXIF = 8
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
			
		default:
			[NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
			
	}
	
	UIGraphicsBeginImageContext(bounds.size);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
		CGContextScaleCTM(context, -scaleRatio, scaleRatio);
		CGContextTranslateCTM(context, -height, 0);
	}
	else {
		CGContextScaleCTM(context, scaleRatio, -scaleRatio);
		CGContextTranslateCTM(context, 0, -height);
	}
	
	CGContextConcatCTM(context, transform);
	
	CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
	//UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	//[self setRotatedImage:imageCopy];
	// return imageCopy;
}






@end
