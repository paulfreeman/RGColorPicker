//
//  HexEntryViewController.m
//  Foliobook
//
//  Created by Paul on 12/01/2014.
//
//

#import "HexEntryViewController.h"

@import foliobookcore;

@interface HexEntryViewController ()

@end

@implementation HexEntryViewController
 

-(void) populateControls {
    
    UIColor *c =  self.textualDelegate.delegate.currentlySelectedColor ;
    if(IsEmpty(c)){
        c = [UIColor whiteColor];
    }
    
    NSString *hexString = [PFLFColor hexStringFromColor:c];
    
    CGFloat red, green, blue, alpha;
    [c getRed:&red green:&green blue:&blue alpha:&alpha];
    
    //hex
    self.tf1.text = hexString;
    
    //alpha
    self.slider1.value = alpha;
    self.tf2.text = [NSString stringWithFormat:@"%2.0f",  alpha*255.0];
    
}

#pragma mark --
#pragma mark text

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(IsEmpty(string)){
        return YES;
    }
    else  if(self.tf1.text.length >= 6){
        return NO;
    }
    else if([string isEqualToString:@"\n"]){
        [textField resignFirstResponder];
        return NO;
    }
    else if( [string rangeOfCharacterFromSet:self.hexCharacterSet].location == NSNotFound) {
        return NO;
    }
    else if(self.tf1.text.length >= 5){
        [self colourChanged:string];
        return YES;
    }
    else {
        return YES;
    }
}
 
-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    if(self.tf1.text.length > 6){
        self.tf1.text = [self.tf1.text substringToIndex:6];
    }
    [textField resignFirstResponder];
    return YES;
}

#pragma mark --
#pragma mark lifecycle

-(void) initHexCharacterSet {
    self.hexCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFabcdef01234567890\n"];
}


-(void) viewWillAppear:(BOOL)animated {
    [self populateControls];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tf1.delegate = self;
    [self initHexCharacterSet];
}


#pragma mark --
#pragma mark actions

-(IBAction) colorChanged   {
    [self colourChanged:nil];
}

-(IBAction) colourChanged:(NSString *) charString {
    NSString *hex ;
    if(!IsEmpty(charString)){
        hex = [NSString stringWithFormat:@"%@%@", self.tf1.text, charString];
    }
    else {
        hex = self.tf1.text;
    }
    UIColor *col = [PFLFColor colorWithHexString:hex];
    CGFloat r,g,b,a ;
    [col getRed:&r green:&g blue:&b alpha:&a];
    UIColor *c = [UIColor colorWithRed :r green :g blue :b alpha:self.slider1.value];
    [self.textualDelegate  colorChanged:c];
}

- (IBAction)slider1changed:(id)sender {
    self.tf2.text = [NSString stringWithFormat:@"%2.0f", self.slider1.value*255.0];
    [self colorChanged];
}

- (IBAction)tf1changed:(id)sender {
    [self colorChanged];
}

- (IBAction)tf2changed:(id)sender {
    CGFloat colourValue = [self.tf2.text floatValue]/255.0;
    self.slider1.value = colourValue;
    [self colorChanged];
}

@end
