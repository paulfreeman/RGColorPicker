//
//  RGTextualColourPickerViewController.m
//  Foliobook
//
//  Created by Paul on 10/01/2014.
//
//

#import "RGTextualColourPickerViewController.h"

@import foliobookcore;
#import "HueSlidersViewController.h"
#import "ColourModelSelectorViewController.h"
#import "RSOpacitySlider.h"
#import "RGPropertyNotification.h"
#import "CoreDataController.h"

#define COLOR_PATCH_CORNER_RADIUS 4

@interface RGTextualColourPickerViewController ()

@end

@implementation RGTextualColourPickerViewController
   

-(void) colorChanged:(UIColor *) color {
    if(!self.notificationsEnabled){
        self.notificationsEnabled = YES;
        return;
    }
    
    self.colorPatchView.backgroundColor = color;
    
    //Share with parent/tab controller
    self.delegate.currentlySelectedColor = color;
    if(self.notificationsEnabled){
        
        if(self.notificationScope == RGPropertyNotificationScopeBackground){
            //Direct update here
            NSString *hexColor = [PFLFColor hexStringFromColor:color];
           [[CoreDataController sharedInstance ] setComponentProp:hexColor forKey:BACKGROUND_COLOR withComponentName:self.selectedComponent.componentName];
           [[NSNotificationCenter defaultCenter] postNotificationName:BACKGROUND_COLOR_CHANGED_NOTIFICATION object:color];
        }
        else if(self.notificationScope == RGPropertyNotificationScopeMenu
            || self.notificationScope == RGPropertyNotificationScopePage){
        
        RGPropertyNotification *cnotify = [RGPropertyNotification propertyNotificationWithColor:color scope: self.notificationScope];
        [[NSNotificationCenter defaultCenter] postNotificationName: RGPropertyNotificationMenuFontColorChanged object:cnotify];
        
        }
        else {
            RGPropertyNotification *notification = [RGPropertyNotification propertyNotificationWithColor:color scope :self.notificationScope];
            [[NSNotificationCenter defaultCenter] postNotificationName:RGPropertyNotificationColorChanged object:notification];
        
        } /*
        else {
        
        RGPropertyNotification *notification = [RGPropertyNotification propertyNotificationWithColor:color scope:self.notificationScope];
        [[NSNotificationCenter defaultCenter] postNotificationName:RGPropertyNotificationColorChanged
                                                            object:notification];
            
        if([RGPropertyNotification propertyNotificationScopeIsMenuType:self.notificationScope] || self.notificationScope == RGPropertyNotificationScopePage){
            [[NSNotificationCenter defaultCenter] postNotificationName: GALLERY_LINK_COLOR_CHANGED_NOTIFICATION object:[PFLFColor hexStringFromColor:color]];
            float alpha,r,g,b;
            [color getRed:&r green:&g  blue:&b  alpha:&alpha ];
            [[NSNotificationCenter defaultCenter] postNotificationName: GALLERY_LINK_ALPHA_CHANGED_NOTIFICATION object:[NSNumber numberWithFloat:alpha]];
        }
           }*/
    }
}


// View that shows selected color
-(void) initColorPatchView {
    //transparent underlay _colorPatch =
    self.colorPatchBackgroundView  = [[UIView alloc] initWithFrame:self.colorPatchView.frame];
    self.colorPatchBackgroundView.layer.cornerRadius = 4;
    [self.view addSubview: self.colorPatchBackgroundView];
    UIImage *backgroundImage = RSOpacityBackgroundImage(16.f, [UIColor colorWithWhite:0.5 alpha:1.0]);
    self.colorPatchBackgroundView.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    self.colorPatchBackgroundView.alpha = 0.2;
    
    self.colorPatchView.layer.cornerRadius=COLOR_PATCH_CORNER_RADIUS;
    [self.view bringSubviewToFront:self.colorPatchView];
}



-(void) viewDidAppear:(BOOL)animated {
    self.notificationsEnabled = YES;
}

-(void) viewWillAppear:(BOOL)animated {
    if(!IsEmpty(self.delegate.currentlySelectedColor)){
        self.colorPatchView.backgroundColor = self.delegate.currentlySelectedColor;
    }
}


-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     ColourModelSelectorViewController *vc = (ColourModelSelectorViewController *) segue.destinationViewController;
    vc.textualDelegate = self;
    if([segue.identifier isEqualToString:@"hsvModelSegue"]){
        self.hsvController = vc;
    }
    else if ([segue.identifier isEqualToString:@"rgbModelSegue"]){
        self.rgbController = vc;
    }
    else if ([segue.identifier isEqualToString:@"webModelSegue"]){
        self.webController = vc;
    }
}


-(void) viewDidLoad {
    self.view.backgroundColor = [UIColor lightGrayColor];
    self.notificationsEnabled = NO;
    [self initColorPatchView];
    [self showHSV];
}

-(void) showRGB {
    [self.rgbController populateControls];
    self.rgbContainerView.hidden = NO;
    self.hsvContainerView.hidden = YES;
    self.webContainerView.hidden = YES;
    self.colorModelSegmentedControl.selectedSegmentIndex = 1;
}

-(void) showHSV {
    [self.hsvController populateControls];
    self.rgbContainerView.hidden = YES;
    self.hsvContainerView.hidden = NO;
    self.webContainerView.hidden = YES;
    self.colorModelSegmentedControl.selectedSegmentIndex = 0;
}

-(void) showWEB {
    [self.webController populateControls];
    self.rgbContainerView.hidden = YES;
    self.hsvContainerView.hidden = YES;
    self.webContainerView.hidden = NO;
    self.colorModelSegmentedControl.selectedSegmentIndex = 2;
}

- (IBAction)colorModelChanged:(id)sender {
    NSInteger model = ((UISegmentedControl *) sender).selectedSegmentIndex;
    switch (model) {
        case RGhsv: [self showHSV]; break;
        case RGrgb: [self showRGB]; break;
        case RGweb: [self showWEB]; break;
        default:
            break;
    }
}

#pragma mark --

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
