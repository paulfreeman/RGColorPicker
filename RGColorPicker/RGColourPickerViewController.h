//
//  RGColourPickerViewController.h
//  Foliobook
//
//  Created by Paul on 10/01/2014.
//
//

#import <UIKit/UIKit.h>
#import "RSColorPickerView.h"
#import "LucidaPortfolioComponent.h"
#import "RGEditorPaneProtocol.h"


@interface RGColourPickerViewController : UITabBarController  <RGEditorPaneProtocol>

@property (nonatomic, strong) UIColor *currentlySelectedColor;
-(void) initSelectedColor:(NSString *) colorHex withTransparency:(NSString *) alpha ;

//RGEditorPaneProtocol Impl
@property(nonatomic, strong) LucidaPortfolioComponent *selectedComponent;
@property enum RGPropertyNotificationScope notificationScope;

@end
