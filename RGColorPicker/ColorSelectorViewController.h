//
//  ColorSelectorViewController.h
//  Foliobook
//
//  Created by Paul on 12/01/2014.
//
//

#import <UIKit/UIKit.h>
#import "RGColourPickerViewController.h"
#import "RGColorController.h"
#import "RGPropertyNotification.h"

@interface ColorSelectorViewController : UIViewController <RGColorController>

@property (nonatomic, weak) RGColourPickerViewController  *delegate;

//RGEditorPaneProtocol Impl
@property(nonatomic, strong) LucidaPortfolioComponent *selectedComponent;
@property enum RGPropertyNotificationScope notificationScope;

@end
