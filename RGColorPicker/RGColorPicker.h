//
//  RGColorPicker.h
//  RGColorPicker
//
//  Created by Paul on 18/04/2015.
//  Copyright (c) 2015 com.rocketgardenlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for RGColorPicker.
FOUNDATION_EXPORT double RGColorPickerVersionNumber;

//! Project version string for RGColorPicker.
FOUNDATION_EXPORT const unsigned char RGColorPickerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RGColorPicker/PublicHeader.h>


