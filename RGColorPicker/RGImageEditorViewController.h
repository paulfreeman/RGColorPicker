//
//  RGImageEditorViewController.h
//  Foliobook
//
//  Created by Paul on 07/03/2014.
//
//

#import <UIKit/UIKit.h>
#import "RGEditorPaneProtocol.h"
#import "RGPickerDelegate.h"
#import "CloudFilePickerDelegate.h"
//#import "EditableComponent.h"
#import "RSOpacitySlider.h"

//BUILD KLUDGE
//Two interfaces kludged in to build with the colour picker included
//I need to work out how to better interface the color picker with
//arbitrary receivers

@interface ParentComponent
@property (strong, nonatomic) NSString  *componentName;
@end

@interface EditableComponent
@property (strong, nonatomic) ParentComponent *parentComponent;
@property (strong, nonatomic) NSString        *componentName;
@end

#define BACKGROUND_IMAGE_ALPHA @"background-image-alpha"
#define SELECTION_BACKGROUND_CHANGED_NOTIFICATION @"selection-background-changed"

//BUILD KLUDGING END


@interface RGImageEditorViewController : UIViewController <RGEditorPaneProtocol, RGPickerDelegate, CloudFilePickerDelegate>

@property (weak, nonatomic) EditableComponent *contextComponent;

@property (weak, nonatomic)   IBOutlet UIImageView         *thumbnailIV;
@property (strong, nonatomic) IBOutlet RSOpacitySlider     *alphaSlider;
@property (weak, nonatomic)   IBOutlet UISegmentedControl  *aspectSegmentedControl;

- (IBAction)selectImage:(id)sender;
- (IBAction)alphaChanged:(id)sender;
- (IBAction)aspectChanged:(id)sender;

@end
