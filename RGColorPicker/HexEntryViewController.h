//
//  HexEntryViewController.h
//  Foliobook
//
//  Created by Paul on 12/01/2014.
//
//

#import <UIKit/UIKit.h>
#import "ColourModelSelectorViewController.h"

@interface HexEntryViewController : ColourModelSelectorViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *tf1;

@property (weak, nonatomic) IBOutlet UISlider *slider1;
@property (weak, nonatomic) IBOutlet UITextField *tf2;

@property (strong, nonatomic) NSCharacterSet *hexCharacterSet;

- (IBAction)slider1changed:(id)sender;

- (IBAction)tf1changed:(id)sender;
- (IBAction)tf2changed:(id)sender;

@end
