//
//  RGColourPickerViewController.m
//  Foliobook
//
//  Created by Paul on 10/01/2014.
//
//

#import "RGColourPickerViewController.h"
#import "RGColorController.h"

@import foliobookcore;
#import "LucidaPortfolioComponent.h"

@interface RGColourPickerViewController ()

@end

@implementation RGColourPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) initSelectedColor:(NSString *) colorHex withTransparency:(NSString *) alpha {
    if(IsEmpty(colorHex)){
        colorHex = @"FFFFFF";
    }
    if(IsEmpty(alpha)){
        alpha = @"1";
    }
    UIColor *solid = [PFLFColor colorWithHexString:colorHex];
    CGFloat hue, saturation, brightness, transparency, transparencyFromInput;
    [solid getHue:&hue saturation:&saturation brightness:&brightness alpha:&transparency];
    transparencyFromInput = [alpha floatValue];
    UIColor *selectionColor = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:transparencyFromInput];
    self.currentlySelectedColor = selectionColor;
}

-(void) initViewControllers {
    UIViewController *vc ;
    id<RGColorController> controller ;
    for( vc  in self.viewControllers) {
        controller = (id<RGColorController>) vc;
        [controller setDelegate:self];
        [controller setSelectedComponent:self.selectedComponent];
        [controller setNotificationScope:self.notificationScope];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initViewControllers];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"Preparing for instantiation of tab view");
    
}
 

@end
