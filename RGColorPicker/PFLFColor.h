//
//  ColorUtils.h
//  LucidaFolio
//
//  Created by Paul Freeman on 21/06/2010.
//  Copyright 2010 Rocket Garden Labs Ltd All rights reserved.
//

#import <Foundation/Foundation.h> 
#import <UIKit/UIKit.h>

#define DEFAULT_VOID_COLOR [UIColor blackColor]
 

@interface PFLFColor : UIColor {
	
}

+ (CGColorSpaceModel)colorSpaceModel:(UIColor *) color; 
+ (BOOL) canProvideRGBComponents :(UIColor *) color;
+ (CGFloat) red :(UIColor *) color;
+ (CGFloat) green :(UIColor *) color;
+ (CGFloat) blue :(UIColor *) color;
+ (CGFloat) alpha: (UIColor *) color;
+ (NSString *) hexStringFromRed:(CGFloat)r green:(CGFloat) g blue:(CGFloat) b; 
+ (NSString *) hexStringFromColor:(UIColor *) color ; 
+ (UIColor *) colorWithHexString: (NSString *) stringToConvert ;
  

@end
 