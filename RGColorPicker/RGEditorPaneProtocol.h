//
//  RGEditorPaneProtocol.h
//  Foliobook
//
//  Created by Paul on 15/01/2014.
//
//

#import <Foundation/Foundation.h>
#import "LucidaPortfolioComponent.h"
#import "RGPropertyNotification.h"

@protocol RGEditorPaneProtocol <NSObject>

//If a single object is selected set it on the editor pane to provide info for populating the pane with its
//current object properties.
-(void) setSelectedComponent:(LucidaPortfolioComponent *) component;
-(void) setNotificationScope:(enum RGPropertyNotificationScope) scope;

@end
