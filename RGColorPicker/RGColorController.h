//
//  RGColorController.h
//  Foliobook
//
//  Created by Paul on 10/01/2014.
//
#import <Foundation/Foundation.h>
#import "RGColourPickerViewController.h"
#import "RGPropertyNotification.h"
#import "RGEditorPaneProtocol.h"

@protocol RGColorController <NSObject, RGEditorPaneProtocol>
-(void) setDelegate:(RGColourPickerViewController *) delegate;
@end
