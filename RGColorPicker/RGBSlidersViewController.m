//
//  RGBSlidersViewController.m
//  Foliobook
//
//  Created by Paul on 12/01/2014.
//
//

#import "RGBSlidersViewController.h"
#import "PFLFColor.h"

@interface RGBSlidersViewController ()

@end

@implementation RGBSlidersViewController

-(void) populateControls {
    
    UIColor *c =  self.textualDelegate.delegate.currentlySelectedColor ;
    if(IsEmpty(c)){
        c = [UIColor whiteColor];
    }
    
    CGFloat red, green, blue, alpha;
    [c getRed:&red green:&green blue:&blue alpha:&alpha];
    
    self.slider1.value = red;
    self.tf1.text = [NSString stringWithFormat:@"%2.0f",  red*255.0];
    
    self.slider2.value = green;
    self.tf2.text = [NSString stringWithFormat:@"%2.0f",  green*255.0];
    
    self.slider3.value = blue;
    self.tf3.text = [NSString stringWithFormat:@"%2.0f",  blue*255.0];
    
    self.slider4.value = alpha;
    self.tf4.text = [NSString stringWithFormat:@"%2.0f",     alpha*255.0];
    
}

#pragma mark --
#pragma mark text


#pragma mark --
#pragma mark lifecycle

-(void) viewWillAppear:(BOOL)animated {
    [self populateControls];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark --
#pragma mark actions


-(IBAction) colourChanged {
    UIColor *c = [UIColor colorWithRed :self.slider1.value green :self.slider2.value blue :self.slider3.value alpha:self.slider4.value];
    [self.textualDelegate  colorChanged:c];
}


- (IBAction)slider1changed:(id)sender {
    self.tf1.text = [NSString stringWithFormat:@"%2.0f", self.slider1.value*255.0];
    [self colourChanged];
}

- (IBAction)slider2changed:(id)sender {
    self.tf2.text = [NSString stringWithFormat:@"%2.0f", self.slider2.value*255.0];
    [self colourChanged];
}

- (IBAction)slider3changed:(id)sender {
    self.tf3.text = [NSString stringWithFormat:@"%2.0f", self.slider3.value*255.0];
    [self colourChanged];
}

- (IBAction)slider4changed:(id)sender {
    self.tf4.text = [NSString stringWithFormat:@"%2.0f", self.slider4.value*255.0];
    [self colourChanged];
}

- (IBAction)tf1changed:(id)sender {
    CGFloat colourValue = [self.tf1.text floatValue]/255.0;
    self.slider1.value = colourValue;
    [self colourChanged];
}

- (IBAction)tf2changed:(id)sender {
    CGFloat colourValue = [self.tf2.text floatValue]/255.0;
    self.slider2.value = colourValue;
    [self colourChanged];
}

- (IBAction)tf3changed:(id)sender {
    CGFloat colourValue = [self.tf3.text floatValue]/255.0;
    self.slider3.value = colourValue;
    [self colourChanged];
}

- (IBAction)tf4changed:(id)sender {
    CGFloat colourValue = [self.tf4.text floatValue]/255.0;
    self.slider4.value = colourValue;
    [self colourChanged];
}



@end
