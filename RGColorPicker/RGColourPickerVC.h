//
//  TestColorViewController.h
//  RSColorPicker
//
//  Created by Ryan Sullivan on 7/14/13.
//  Copyright (c) 2013 Freelance Web Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSColorPickerView.h"
#import "RGColorController.h"
#import "ColorSelectorViewController.h"
#import "RGPropertyNotification.h"


@class RSBrightnessSlider;
@class RSOpacitySlider;

@interface RGColourPickerVC : ColorSelectorViewController <RSColorPickerViewDelegate> {
    BOOL isSmallSize;
} 

@property BOOL notificationsEnabled;

@property (nonatomic) RSColorPickerView  *colorPicker;
@property (nonatomic) RSBrightnessSlider *brightnessSlider;
@property (nonatomic) RSOpacitySlider    *opacitySlider;
@property (nonatomic) UIView             *colorPatch;

@property (nonatomic) UIView             *colorPatchBackgroundView;

@property UILabel *rgbLabel;

@end
